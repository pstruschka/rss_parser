#!/usr/bin/env python3

import logging
from typing import List

import pika

LOG = logging

LOG.basicConfig(
    level=LOG.DEBUG, format='%(name)-32s - %(levelname)-8s - %(message)-s')

LOG.getLogger('pika').setLevel(logging.WARNING)


class Rabbit:
    def __init__(self, host, port, instance, attempts=5, delay=3):
        self._log = logging.getLogger('rabbit')
        self._host = host
        self._port = int(port)
        self._instance = instance
        self.connection = pika.BlockingConnection(
            pika.ConnectionParameters(
                host=self._host,
                port=self._port,
                connection_attempts=attempts,
                retry_delay=delay))
        self._channel = self.connection.channel()
        self._channel.basic_qos(prefetch_count=1)

    def define_queues(self, queues: List = [], durable_queues: List = []):
        if not isinstance(queues, list) or not isinstance(
                durable_queues, list):
            return TypeError('List of queues required')
        for queue in queues:
            self._channel.queue_declare(queue=queue)
        for queue in durable_queues:
            self._channel.queue_declare(queue=queue, durable=True)

    def define_consumer(self, queue, callback, **kwargs):
        if not all((k in kwargs.keys()) for k in ['logger']):
            raise TypeError('Logger required')
        self._channel.basic_consume(
            lambda ch, method, properties, body:
            callback(ch, method, properties, body, self, **kwargs),
            queue=queue, consumer_tag=self._instance)

    def publish_message(channel, queue, body):
        channel.basic_publish(exchange='', routing_key=queue, body=body)

    def start_consuming(self):
        self._channel.start_consuming()
