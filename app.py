#!/usr/bin/env python3

import logging
import os
import uuid
import pprint
import base64
import json
import time

from elastic_search import ES
from rssparser import FeedParser
from rabbit import Rabbit
from mongo import Mongo

LOG = logging

RABBIT_HOST = os.getenv('RABBIT_HOST', 'localhost')
RABBIT_PORT = os.getenv('RABBIT_PORT', '5672')

WORK_QUEUE = os.getenv('WORK_QUEUE', 'rss')
STATUS_QUEUE = os.getenv('STATUS_QUEUE', 'rss_status')
SENTIMENT_ANALYSIS_QUEUE = os.getenv('SENTIMENT_ANALYSIS_QUEUE', 'sentiment')

ES_HOST = os.getenv('ES_HOST', 'localhost:9200')

MONGO_HOST = os.getenv('MONGO_HOST', 'localhost')
MONGO_PORT = os.getenv('MONGO_PORT', '27017')

INSTANCE_NAME = uuid.uuid4().hex

LOG.basicConfig(
    level=LOG.DEBUG, format='%(name)-32s - %(levelname)-8s - %(message)-s')

LOG.getLogger('urllib3.connectionpool').setLevel(LOG.WARNING)
LOG.getLogger('rssparser').setLevel(LOG.DEBUG)


def handle(channel, method, properties, body, rabbit: Rabbit, logger,
           mongo: Mongo, es: ES):
    rss_url = body.decode('UTF-8')
    logger.info('Recieved : %r', rss_url)
    try:
        fd = FeedParser(rss_url)
    except RuntimeError as e:
        logger.warning('Failed to parse %s with error %r', rss_url, e.args[0])
        Rabbit.publish_message(
            channel, STATUS_QUEUE,
            json.dumps({
                'status': 'error',
                'rss': rss_url,
                'message': str(e.args[0])
            }))
        channel.basic_ack(delivery_tag=method.delivery_tag)
        return

    index = base64.b32encode(body)
    index_str = index.decode('UTF-8')

    logger.info('Base32 encoded rss  : %s', index_str)

    # Create elasticsearch index
    # es.create_index(index.lower())

    last_time_doc = mongo.find('rss_parser', 'last_time',
                               {index_str: {
                                   '$exists': True
                               }})

    # time the last article was published
    # articles must be newer than this time
    last_time = last_time_doc[index_str] if last_time_doc else 0
    latest_time = last_time

    total = len(fd)

    if total == 0:
        logger.warning('Empty Feed: %r', rss_url)
        Rabbit.publish_message(
            channel, STATUS_QUEUE,
            json.dumps({
                'status': 'error',
                'rss': rss_url,
                'message': 'Empty Feed'
            }))
        channel.basic_ack(delivery_tag=method.delivery_tag)
        return

    for summary in fd.summaries(after=last_time):
        logger.debug(pprint.pformat(summary))
        latest_time_tmp = max(latest_time,
                              time.mktime(summary.published_parsed))
        if latest_time != latest_time_tmp:
            latest_time = latest_time_tmp

    if latest_time > last_time:
        mongo.update('rss_parser', 'last_time', {index_str: {
            '$exists': True
        }}, {'$set': {
            index_str: latest_time
        }})

    count = 0
    for link, title, published, entry in fd.articles(delay=2, after=last_time):
        logger.info(' %d/%d', count + 1, total)
        logger.info(' TITLE : %s', title)
        logger.info('   len : %d', len(entry))

        if len(entry) > 500:
            logger.info('       : Added')

            # document = es.add_document(
            #     index,
            #     doc_type='article',
            #     link=link,
            #     title=title,
            #     article=entry)
            document_id = mongo.insert('articles', index_str, {
                'title': title,
                'link': link,
                'article': entry
            })

            logger.info('       : %s', document_id)
            Rabbit.publish_message(
                channel, SENTIMENT_ANALYSIS_QUEUE,
                json.dumps({
                    'collection': index_str,
                    'id': str(document_id),
                }))
            count += 1

        else:
            logger.info('       : Ignored')
        rabbit.connection.process_data_events()  # Send heartbeats

    # es.refresh_index(index)

    channel.basic_ack(delivery_tag=method.delivery_tag)

    logger.info(' Done  : %d/%d', count, total)
    logger.info('       :          : %s', rss_url)
    logger.info('       : Index    : %s', index_str)
    logger.info('       : Complete :')
    # logger.info('%s', pprint.pformat(es.list_index(index_str)))


def main():
    LOG.info('Started process')
    LOG.info('Identifier : %s', INSTANCE_NAME)

    named_logging = LOG.getLogger(name=INSTANCE_NAME)

    rabbit = Rabbit(RABBIT_HOST, RABBIT_PORT, INSTANCE_NAME)
    named_logging.info('Connected to RabbitMQ on %s', RABBIT_HOST)

    es = None  # ES(ES_HOST)

    mongo = Mongo(MONGO_HOST, int(MONGO_PORT))

    rabbit.define_queues(
        queues=[WORK_QUEUE, STATUS_QUEUE, SENTIMENT_ANALYSIS_QUEUE])
    rabbit.define_consumer(
        WORK_QUEUE, handle, logger=named_logging, mongo=mongo, es=es)

    named_logging.info('Start consuming')
    rabbit.start_consuming()


if __name__ == '__main__':
    main()
