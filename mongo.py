from pymongo import MongoClient


class Mongo():
    def __init__(self, host, port):
        self.client = MongoClient(host, port)

    def _collection(self, database, collection):
        return self.client[database][collection]

    def insert(self, db, col, doc):
        collection = self._collection(db, col)

        result = collection.insert_one(doc)
        return result.inserted_id

    def insert_many(self, db, col, data):
        collection = self._collection(db, col)

        result = collection.insert_many(data)
        return result.inserted_ids

    def find(self, db, col, query):
        collection = self._collection(db, col)

        return collection.find_one(query)

    def update(self, db, col, query, update):
        collection = self._collection(db, col)

        result = collection.update_one(query, update, upsert=True)
        return result

    # db = articles

    # collection = base32encoded rss string

    # doc = {article_title: title, article_title}

    # collecion: "aoeuoaeuao"
    # id: "aoueou"
