import logging
import uuid

from elasticsearch import Elasticsearch

logging.getLogger('elasticsearch').setLevel(logging.WARNING)
LOG = logging.getLogger('elastic_search')


class ES:
    def __init__(self, host):
        self._host = host
        self._es = Elasticsearch(hosts=[host])
        self._DOC_TYPE = 'article'

    def _index_exists(self, index):
        return self._es.indices.exists(index=index)

    def create_index(self, index=None):
        if not index:
            index = uuid.uuid4().hex

        if not self._index_exists(index):
            self._es.indices.create(index=index)

    def delete_index(self, index):
        if self._index_exists(index):
            self._es.indices.delete(index=index)

    def refresh_index(self, index):
        if self._index_exists(index):
            self._es.indices.refresh(index=index)

    def add_document(self, index, doc_type=None, **kwargs):
        if not doc_type:
            doc_type = self._DOC_TYPE
        if self._index_exists(index):
            return self._es.index(index=index, doc_type=doc_type, body=kwargs)

    def list_index(self, index):
        if self._index_exists(index):
            res = self._es.search(
                index=index, body={'query': {
                    'match_all': {}
                }})
            return res['hits']['hits']
