import feedparser
import requests
import time
import logging
import pprint
from lxml import html

LOG = logging.getLogger('rssparser')


class _Article:
    # Only good at the time this was written.
    # Changes may be made in future.
    # May not work for old or future articles.
    ARTICLE_LOCATION = {
        'bbc.co': ('story-body__inner', 'p'),
        'bangkokpost.com': ('articleContents', 'p'),
        'economist.co': ('blog-post__text', 'p'),
        'theguardian.co': ('content__article-body', 'p'),
    }

    def __init__(self, url):
        super(_Article, self).__init__()
        self._url = url
        self._page = requests.get(url).text
        self._tree = html.fromstring(self._page)

    def FromContent(self, url, content):
        super(_Article, self).__init__()
        self._url = url
        self._page = content
        self._tree = html.fromstring(content)
        return self

    def get_article(self):
        for k in self.ARTICLE_LOCATION:
            LOG.debug("%s %s %r", k, self._url, k in self._url)
            if k in self._url:
                css_class, text_elem = self.ARTICLE_LOCATION[k]
                body = self._tree.find_class(css_class)
                article = ""
                for b in body:
                    p = b.cssselect(text_elem)
                    for pp in p:
                        article += (pp.text_content() + '\n')
                return article.strip()
        raise ValueError


class FeedParser:
    def __init__(self, url):
        super(FeedParser, self).__init__()
        self._url = url
        LOG.debug(url)
        self._data = feedparser.parse(url)
        LOG.debug(pprint.pformat(self._data))
        if self._data['bozo'] == 1:
            raise RuntimeError(self._data['bozo_exception'])

    def __len__(self):
        return len(self._data.entries)

    def articles(self, delay=5, after=0.0):
        for entry in self._data.entries:
            if time.mktime(entry.published_parsed) <= after:
                LOG.debug('Ignored old article %s', entry.title)
                continue

            if hasattr(entry, 'content'):  # Atom feed
                article = _Article.FromContent(entry.content)
                yield (entry.link, entry.title,
                       time.mktime(entry.published_parsed),
                       article.get_article())
            else:
                article = _Article(entry.link)
                yield (entry.link, entry.title, entry.published_parsed,
                       article.get_article())
                time.sleep(delay)

    def summaries(self, after=0.0):
        LOG.debug('len: %d', len(self._data.entries))
        for entry in self._data.entries:
            if time.mktime(entry.published_parsed) <= after:
                continue
            yield entry
