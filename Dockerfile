FROM python:3.7-alpine

RUN apk add --update --no-cache g++ gcc libxslt-dev
RUN pip install pipenv

WORKDIR /app

COPY Pipfile* ./

RUN pipenv install --system --deploy --ignore-pipfile

copy *.py ./

CMD ["python", "app.py"]
